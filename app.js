/**
 * INCLUDE LIBRARY
 */
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const config = require('./config'); // get our config file
const Users = require('./server/models').Users;
const app = express();
const cors = require('cors');
const helmet = require('helmet');
const md5 = require('md5');
const expressValidator = require('express-validator');
/**
 * 
 * KONFIGURASI 
 */
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());
app.use(helmet());
app.set('secret', config.secret);
app.set('baseUrl', config.baseurl);
app.set('userid', config.userid); // secret variable
app.set('password', config.password); // secret variable
app.disable('x-powered-by');

require('./server/routes/public')(app);

require('./server/routes/protected')(app);

app.use((err, req, res, next) => {

    res.status(400).json({ msg: 'bad request' });
});
module.exports = app;