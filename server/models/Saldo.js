module.exports = (sequelize, DataTypes) => {
    const Saldo = sequelize.define('Saldo', {
        biller: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        saldo: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        }
    }, {
        tableName: 'saldo',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Saldo;
};