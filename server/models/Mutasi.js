module.exports = (sequelize, DataTypes) => {
    const Mutasi = sequelize.define('Mutasi', {
        userid: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        nominal: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        saldoawal: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        saldoakhir: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        time_stamp: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        }
    }, {
        tableName: 'mutasi',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Mutasi;
};