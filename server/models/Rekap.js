module.exports = (sequelize, DataTypes) => {
    const Rekap = sequelize.define('Rekap', {
        ntrans: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        mssidn: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        kode: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        response: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        timestamp: {
            type: DataTypes.DATE,
            defaultValue: sequelize.NOW
        },
        status: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        ketstatus: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }, {
        tableName: 'rekap',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Rekap;
};