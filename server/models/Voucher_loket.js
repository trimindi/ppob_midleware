module.exports = (sequelize, DataTypes) => {
    const Voucher_loket = sequelize.define('Voucher_loket', {
        kode: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        fk_kategori: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        nama: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        harga: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
        fee: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
        fee_loket: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
        admin: {
            type: DataTypes.DOUBLE,
            allowNull: false,
        },
        is_enable: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        fk_user: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    }, {
        tableName: 'voucher_loket',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Voucher_loket;
};