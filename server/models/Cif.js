module.exports = (sequelize, DataTypes) => {
    const Cif = sequelize.define('Cif', {
        CIF: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        NAMA_CLIENT: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        NO_SPK: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        AKTA: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ALAMAT: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        LONGITUDE: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        LATTITUDE: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        TELEPON: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        FAX: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        EMAIL: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        WEBSITE: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        IDCAB: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ID_PROP: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ID_KAB: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ID_KEC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ID_KEL: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        KODE_POS: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        NAMA_KETUA: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        TELEPON_KETUA: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        NAMA_WAKIL_KETUA: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        TELEPON_WAKIL_KETUA: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        NAMA_BENDAHARA: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        TELEPON_BENDAHARA: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        NAMA_PIC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        TELEPON_PIC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        EMAIL_PIC: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ANGGOTA: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        IP_ADDRESS: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        TGL_CUTOFF: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        BLN_CUTOFF: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        THN_CUTOFF: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        TGL_REG: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        TGL_APV: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        TGL_ACT: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        TGL_PBL: {
            type: DataTypes.DATE,
            allowNull: false,
        },
        KEY_REGISTRASI: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        ST: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    }, {
        tableName: 'cif',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Cif.removeAttribute('id');
    return Cif;
};