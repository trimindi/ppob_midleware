module.exports = (sequelize, DataTypes) => {
    const Kategori = sequelize.define('Kategori', {
        nama: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        golongan: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        is_enable: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        }

    }, {
        tableName: 'kategori',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    return Kategori;
};