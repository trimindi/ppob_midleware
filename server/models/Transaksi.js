module.exports = (sequelize, DataTypes) => {
    const Transaksi = sequelize.define('Transaksi', {
        ntrans: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        nominal: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        st: {
            type: DataTypes.BOOLEAN,
            allowNull: true,
        },
        fk_user: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        kode: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        keterangan: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        jenis: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        produk: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        mssidn: {
            type: DataTypes.STRING,
            allowNull: true
        },
        kembali: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        pembayaran: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        fee_loket: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        fee_agent: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        saldo: {
            type: DataTypes.DOUBLE,
            allowNull: true,
        },
        jenis:{
          type: DataTypes.STRING,
          allowNull: true,
        },
        produk : {
          type: DataTypes.STRING,
          allowNull: true,
        },
        tanggal: {
            type: DataTypes.DATEONLY,
            defaultValue: function() {
                var date = new Date();
                return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
            },
        },
        waktu: {
            type: DataTypes.TIME,
            defaultValue: function() {
                var date = new Date();
                return date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
            },
        },
        admin: {
            type: DataTypes.DOUBLE,
            allowNull: true
        },
        msg: {
            type: DataTypes.TEXT,
            allowNull: true,
        },
        time_stamp: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'transaksi',
        timestamps: false,
        createdAt: false,
        updateAt: false,
        paranoid: true,
        freezeTableName: false,
    });
    Transaksi.removeAttribute('id');
    return Transaksi;
};
