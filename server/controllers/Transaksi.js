const Transaksi = require('../models').Transaksi;

module.exports = {
    create(req, res) {
        Transaksi
            .create(req.body).then((result) => {
                res.status(200).json({ status: true, msg: 'Data Transaksi Berhasil di buat' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Transaksi Gagal di buat' })
            })
    },
    listOne(req, res) {
        Transaksi
            .find({ where: { ntrans: req.params.ntrans } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Transaksi tidak di temukan' })
            })
    },
    listAll(req, res) {
        console.log("admin" + req.decoded.is_admin)
        if (req.decoded.is_admin == 1) {
            Transaksi
                .findAll().then((result) => {
                    res.status(200).json({ status: true, data: result })
                }).catch((err) => {
                    res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
                })
        } else {
            Transaksi
                .findAll({ where: { fk_user: req.decoded.id } }).then((result) => {
                    res.status(200).json({ status: true, data: result })
                }).catch((err) => {
                    res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
                })
        }

    },
};