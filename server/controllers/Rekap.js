const Rekap = require('../models').Rekap;

module.exports = {
    listOne(req, res) {
        Rekap
            .find({ where: { ntrans: req.params.ntrans } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Rekap tidak di temukan' })
            })
    },
    listAll(req, res) {
        Rekap
            .findAll().then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
};