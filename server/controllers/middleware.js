const Rekap = require('../models').Rekap;
const Voucher = require('../models').Voucher;
const Transaksi = require('../models').Transaksi;
const Saldo = require('../models').Saldo;
const Users = require('../models').Users;
const md5 = require('md5');
const config = require('../../config');
const unirest = require('unirest');

var topup = (mssidn, denom, ntrans) => {
    return new Promise((resolve, reject) => {
        let req = unirest("GET", config.baseUrl);
        req.query({
            "regid": ntrans,
            "userid": config.userid,
            "passwd": config.password,
            "msisd": mssidn,
            "denom": denom
        });

        req.headers({
            "cache-control": "no-cache"
        });

        req.end(function(res) {
            if (res.error) return reject(res.error);
            return resolve(res.body);
        });
    })
}

var checkStatus = (status) => {
    switch (status) {
        case 1:
            return 'SUKSES';
            break;
        case 2:
            return 'Sedang Dalam Proses';
            break;
        case 3:
            return 'Gagal Transaksi';
            break;
        case 4:
            return 'Gagal Saldo Tidak Cukup';
            break;
        case 5:
            return 'Double Transaksi';
            break;
        case 6:
            return 'UserID/Passwd Salah';
            break;
        case 7:
            return 'SUSPECT';
            break;

    }
}

module.exports = {
    inquiry(req, res) {
        if (req.body.kode == undefined || req.body.mssidn == undefined) {
            res.status(200).json({
                status: false,
                msg: 'request parameter ada yang kosong'
            });
            return;
        }

        var saldo;
        var voucher;
        Users.find({
            where: {
                id: req.decoded.id
            }
        }).then(result => {
            if (result == null) {
                res.status(200).json({
                    status: false,
                    msg: 'User not registerd'
                });
                return;
            }
            var users = result;
            var harga;
            Voucher.find({
                    where: {
                        kode: req.body.kode,
                        fk_kategori: req.body.kategori
                    }
                })
                .then(result => {
                    if (result == null) {
                        res.status(200).json({
                            status: false,
                            msg: 'Voucher not registerd'
                        });
                        return;
                    }
                    voucher = result;
                    harga = voucher.harga + voucher.fee + (req.body.admin == undefined ? voucher.admin : req.body.admin);

                    let data = JSON.parse('{"status":1,"timestamp":20170217000000,"tn":"12312","reg_id":"12312","kode_produk":"CIPLN20","sn":"0","message":"SUGIONO.12312","harga":"0","saldo":"0"}');

                    data.reg_id = req.ntrans;
                    data.message = undefined;
                    data.kode_produk = 'CIPLN' + req.body.kode;
                    data.msg = 'TRIMINDI.' + req.body.mssidn;
                    data.harga = voucher.harga;
                    data.fee_loket = voucher.fee_loket;
                    data.fee_agent = voucher.fee_agent;
                    data.admin = voucher.admin;
                    data.saldo = users.saldo;
                    Rekap
                        .create({
                            ntrans: req.ntrans,
                            response: JSON.stringify(data),
                            mssidn: req.body.mssidn,
                            kode: data.kode_produk,
                            status: data.status,
                            ketstatus: checkStatus(data.status)
                        }).then((result) => {
                            res.status(200).json({
                                status: true,
                                data: data
                            });
                        }).catch((err) => {
                            console.log(err)
                        });

                }).catch(err => console.log(err));
        }).catch(err => console.log(err));

    },

    /**
     *
     * {
     *  kategori : '',
     *  kode : '',
     *  mssidn : '',
     *  admin : '',
     *  pembayaran : '',
     *  kembali : '',
     *  fee_loket : ''
     * }
     *
     *
     */
    pulsa(req, res) {
        if (req.body.kode == undefined || req.body.kategori == undefined || req.body.mssidn == undefined) {
            res.status(200).json({
                status: false,
                msg: 'request parameter ada yang kosong'
            });
            return;
        }

        var saldo;
        var voucher;
        Users.find({
            where: {
                id: req.decoded.id
            }
        }).then(result => {
            if (result == null) {
                res.status(200).json({
                    status: false,
                    msg: 'User not registerd'
                });
                return;
            }
            var users = result;
            var harga;
            Voucher.find({
                    where: {
                        kode: req.body.kode,
                        fk_kategori: req.body.kategori
                    }
                })
                .then(result => {
                    if (result == null) {
                        res.status(200).json({
                            status: false,
                            msg: 'Voucher not registerd'
                        });
                        return;
                    }
                    voucher = result;
                    harga = voucher.harga + voucher.fee + req.body.admin;
                    Saldo.find({
                        where: {
                            id: '0000000002'
                        }
                    }).then(result => {
                        saldo = result;
                        if (voucher.harga > saldo.saldo) {
                            res.status(200).json({
                                status: false,
                                msg: "terjadi masalah dengan server harap hubungu CS"
                            });
                            return;
                        }
                        if (users.saldo < (voucher.harga + voucher.fee + (parseInt(req.body.admin) - parseInt(req.body.fee_loket)))) {
                            res.status(200).json({
                                status: false,
                                msg: "saldo anda tidak mencukupi untuk melakukan transaksi Rp." + users.saldo
                            });
                            return;
                        }

                        var date = new Date();
                        Transaksi
                            .create({
                                ntrans: req.ntrans,
                                nominal: harga,
                                mssidn: req.body.mssidn,
                                fk_user: req.decoded.id,
                                kode: req.body.kode,
                                keterangan: voucher.nama,
                                pembayaran: req.body.pembayaran,
                                kembali: req.body.kembali,
                                fee_loket: req.body.fee_loket || voucher.fee_loket,
                                fee_agent: voucher.fee,
                                jenis: req.body.jenis,
                                produk : req.body.produk,
                                admin: req.body.admin || 0,
                                saldo: users.saldo,
                                st: 0
                            }).then(result => {
                                topup(req.body.mssidn, req.body.kode, req.ntrans)
                                    .then(result => {
                                        let data = JSON.parse(result);
                                        if (data.status <= 2) {
                                            data.newsaldo = users.saldo - harga;
                                        } else {
                                            data.newsaldo = users.saldo
                                        }
                                        Transaksi
                                            .update({
                                                st: (data.status == 1 ? 1 : (data.status == 2 ? 0 : 2)),
                                                saldo: data.newsaldo,
                                                msg: data.message
                                            }, {
                                                where: {
                                                    ntrans: req.ntrans
                                                }
                                            }).then(res => {
                                                if (data.status <= 2) {
                                                    Users
                                                        .update({
                                                            saldo: data.newsaldo
                                                        }, {
                                                            where: {
                                                                id: req.decoded.id
                                                            }
                                                        })
                                                        .then(result => {

                                                        }).catch(err => console.log(err));
                                                    Saldo
                                                        .update({
                                                            saldo: saldo.saldo - voucher.harga
                                                        }, {
                                                            where: {
                                                                id: '0000000002'
                                                            }
                                                        })
                                                        .catch(err => console.log(err));
                                                }
                                            }).catch(err => console.log(err));
                                        Rekap
                                            .create({
                                                ntrans: req.ntrans,
                                                response: result,
                                                mssidn: req.body.mssidn,
                                                kode: req.body.kode,
                                                status: data.status,
                                                ketstatus: checkStatus(data.status)
                                            }).then((result) => {

                                            }).catch((err) => {
                                                console.log(err)
                                            });
                                        let reposnse = JSON.parse(result);
                                        res.status(200).json({
                                            status: reposnse.status,
                                            timestamp: reposnse.timestamp,
                                            tn: reposnse.tn,
                                            reg_id: reposnse.reg_id,
                                            sn: reposnse.sn,
                                            msg: reposnse.message,
                                            harga: reposnse.harga,
                                            saldo: users.saldo - harga
                                        });
                                    }).catch((err) => {
                                        res.status(304).json({
                                            'status': 5,
                                            'massage': 'Terjadi Masalah dengan server'
                                        });
                                    });
                            }).catch(err => console.log(err));
                    }).catch(err => console.log(err));
                }).catch(err => console.log(err));
        }).catch(err => console.log(err));
    },
    payment(req, res) {
        req.body.pembayaran = parseInt(req.body.pembayaran);
        req.body.admin = parseInt(req.body.admin);
        req.body.fee_loket = parseInt(req.body.fee_loket);
        req.body.kembali = parseInt(req.body.kembali);
        if (req.body.kode == undefined || req.body.mssidn == undefined) {
            res.status(200).json({
                status: false,
                msg: 'request parameter ada yang kosong'
            });
            return;
        }
        var users;
        var voucher;
        Rekap.find({where:{ ntrans : req.body.ntrans }}).then(result => {
            if(result == null){
              res.status(200).json({
                statu:false,
                msg: "nomor transaksi tidak terdaftar"
              });
              return;
            }

            Users.find({
                where: {
                    id: req.decoded.id
                }
            }).then(result => {
                if (result == null) {
                    res.status(200).json({
                        status: false,
                        msg: 'User not registerd'
                    });
                    return;
                }
                users = result;
                Voucher.find({
                    where: {
                        kode: req.body.kode,
                        fk_kategori: req.body.kategori
                    }
                }).then(result => {
                    if (result == null) {
                        res.status(200).json({
                            status: false,
                            msg: 'voucher not registerd'
                        });
                        return;
                    }
                    voucher = result;
                    Saldo.find({
                        where: {
                            id: '0000000002'
                        }
                    }).then(result => {
                        saldo = result;
                        if ((voucher.harga + voucher.fee_agen) > saldo.saldo) {
                            res.status(200).json({
                                status: false,
                                msg: "terjadi masalah dengan server harap hubungu CS"
                            });
                            return;
                        }
                        if (users.saldo < (voucher.harga + voucher.fee + parseInt(req.body.admin) - parseInt(req.body.fee_loket) )) {
                            res.status(200).json({
                                status: false,
                                msg: "saldo anda tidak mencukupi untuk melakukan transaksi Rp." + users.saldo
                            });
                            return;
                        }
                        let harga = voucher.harga + voucher.fee + req.body.admin;
                        Transaksi
                            .create({
                                ntrans: req.ntrans,
                                nominal: harga,
                                mssidn: req.body.mssidn,
                                fk_user: req.decoded.id,
                                kode: req.body.kode,
                                keterangan: voucher.nama,
                                pembayaran: req.body.pembayaran,
                                kembali: req.body.kembali,
                                fee_loket: req.body.fee_loket || voucher.fee_loket,
                                fee_agent: voucher.fee,
                                jenis: req.body.jenis,
                                produk : req.body.produk,
                                admin: req.body.admin || 0,
                                saldo: users.saldo,
                                st: 0
                            }).then(result => {
                              let response = JSON.parse('{"status":1,"timestamp":20170217000000,"tn":"000000","reg_id":"12312","kode_produk":"BIPLN20","sn":"1234-0987-6543-2109","message":"86012266226 SUKSES NomorToken =1234-0987-6543-2109-0242 Ket=DUMMY TESTING MAULANA/R1/1300/12,9KWH.BIPLN20","harga":"0","saldo":"0"}');

                              response.reg_id = req.body.ntrans;
                              response.kode_produk = 'BIPLN' + req.body.kode;
                              response.message = undefined;
                              response.msg = req.body.mssidn + ' SUKSES NomorToken =1234-0987-6543-2109-0242 Ket=DUMMY TESTING MAULANA/R1/1300/12,9KWH.' + response.kode_produk;
                              response.saldo = users.saldo;
                              let harga = req.body.admin + voucher.harga + voucher.fee;
                              response.harga = harga;
                              if (response.status <= 2) {
                                  response.saldo = users.saldo - harga;
                              } else {
                                  response.saldo = users.saldo
                              }
                              Transaksi
                                  .update({
                                      st: (response.status == 1 ? 1 : (response.status == 2 ? 0 : 2)),
                                      saldo: response.saldo,
                                      msg: response.msg
                                  }, {
                                      where: {
                                          ntrans: req.body.ntrans
                                      }
                                  }).then(res => {
                                      if (response.status <= 2) {
                                          Users
                                              .update({
                                                  saldo: response.saldo
                                              }, {
                                                  where: {
                                                      id: req.decoded.id
                                                  }
                                              })
                                              .then(result => {

                                              }).catch(err => console.log(err));
                                          Saldo
                                              .update({
                                                  saldo: saldo.saldo - (voucher.harga + parseInt(req.body.admin - req.body.fee_loket))
                                              }, {
                                                  where: {
                                                      id: '0000000002'
                                                  }
                                              })
                                              .catch(err => console.log(err));
                                      }
                                  }).catch(err => console.log(err));
                              Rekap
                                  .create({
                                      ntrans: req.body.ntrans,
                                      response: JSON.stringify(response),
                                      mssidn: req.body.mssidn,
                                      kode: req.body.kode,
                                      status: response.status,
                                      ketstatus: checkStatus(response.status)
                                  }).then((result) => {

                                  }).catch((err) => {
                                      console.log(err)
                                  });
                              res.status(200).json({
                                  status:true,
                                  data : response
                              });

                        }).catch(err =>console.log(err));

                    }).catch(err => console.log(err));

                }).catch(err => console.log(err));

            }).catch(err => console.log(err));

        }).catch(err => console.log(err));

    },
    paketdata(req, res) {
        if (req.body.kode == undefined || req.body.kategori == undefined || req.body.mssidn == undefined) {
            res.status(200).json({
                status: false,
                msg: 'request parameter ada yang kosong'
            });
            return;
        }

        var saldo;
        var voucher;
        Users.find({
            where: {
                id: req.decoded.id
            }
        }).then(result => {
            if (result == null) {
                res.status(200).json({
                    status: false,
                    msg: 'User not registerd'
                });
                return;
            }
            var users = result;
            var harga;
            Voucher.find({
                    where: {
                        kode: req.body.kode,
                        fk_kategori: req.body.kategori
                    }
                })
                .then(result => {
                    if (result == null) {
                        res.status(200).json({
                            status: false,
                            msg: 'Voucher not registerd'
                        });
                        return;
                    }
                    voucher = result;
                    harga = voucher.harga + voucher.fee + req.body.admin;
                    Saldo.find({
                        where: {
                            id: '0000000002'
                        }
                    }).then(result => {
                        saldo = result;
                        if (voucher.harga > saldo.saldo) {
                            res.status(200).json({
                                status: false,
                                msg: "terjadi masalah dengan server harap hubungu CS"
                            });
                            return;
                        }
                        if (users.saldo < (voucher.harga + voucher.fee + req.body.admin)) {
                            res.status(200).json({
                                status: false,
                                msg: "saldo anda tidak mencukupi untuk melakukan transaksi Rp." + users.saldo
                            });
                            return;
                        }

                        Transaksi
                            .create({
                                ntrans: req.ntrans,
                                nominal: harga,
                                mssidn: req.body.mssidn,
                                fk_user: req.decoded.id,
                                kode: req.body.kode,
                                keterangan: voucher.nama,
                                pembayaran: req.body.pembayaran,
                                kembali: req.body.kembali,
                                fee_loket: req.body.fee_loket || voucher.fee_loket,
                                fee_agent: voucher.fee,
                                jenis: req.body.jenis,
                                produk : req.body.produk,
                                admin: req.body.admin || 0,
                                saldo: users.saldo,
                                st: 0
                            }).then(result => {
                                topup(req.body.mssidn, req.body.kode, req.ntrans)
                                    .then(result => {
                                        let data = JSON.parse(result);
                                        if (data.status <= 2) {
                                            data.newsaldo = users.saldo - harga;
                                        } else {
                                            data.newsaldo = users.saldo
                                        }
                                        Transaksi
                                            .update({
                                                st: (data.status == 1 ? 1 : (data.status == 2 ? 0 : 2)),
                                                saldo: data.newsaldo,
                                                msg: data.message
                                            }, {
                                                where: {
                                                    ntrans: req.ntrans
                                                }
                                            }).then(res => {
                                                if (data.status <= 2) {
                                                    Users
                                                        .update({
                                                            saldo: data.newsaldo
                                                        }, {
                                                            where: {
                                                                id: req.decoded.id
                                                            }
                                                        })
                                                        .then(result => {

                                                        }).catch(err => console.log(err));
                                                    Saldo
                                                        .update({
                                                            saldo: saldo.saldo - voucher.harga
                                                        }, {
                                                            where: {
                                                                id: '0000000002'
                                                            }
                                                        })
                                                        .catch(err => console.log(err));
                                                }
                                            }).catch(err => console.log(err));
                                        Rekap
                                            .create({
                                                ntrans: req.ntrans,
                                                response: result,
                                                mssidn: req.body.mssidn,
                                                kode: req.body.kode,
                                                status: data.status,
                                                ketstatus: checkStatus(data.status)
                                            }).then((result) => {

                                            }).catch((err) => {
                                                console.log(err)
                                            });
                                        let reposnse = JSON.parse(result);
                                        res.status(200).json({
                                            status: reposnse.status,
                                            timestamp: reposnse.timestamp,
                                            tn: reposnse.tn,
                                            reg_id: reposnse.reg_id,
                                            sn: reposnse.sn,
                                            msg: reposnse.message,
                                            harga: reposnse.harga,
                                            saldo: users.saldo - harga
                                        });
                                    }).catch((err) => {
                                        res.status(304).json({
                                            'status': 5,
                                            'massage': 'Terjadi Masalah dengan server'
                                        });
                                    });
                            }).catch(err => console.log(err));
                    }).catch(err => console.log(err));
                }).catch(err => console.log(err));
        }).catch(err => console.log(err));
    },
};
