const Voucher = require('../models').Voucher;

module.exports = {
    create(req, res) {
        Voucher
            .create(req.body).then((result) => {
                res.status(200).json({ status: true, msg: 'Data Voucher Berhasil di buat' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Voucher Gagal di buat' })
            })
    },
    update(req, res) {
        Voucher
            .update(req.body, { where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, msg: 'Data Voucher Berhasil di update' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Voucher Gagal di update' })
            })
    },
    delete(req, res) {
        Voucher
            .destroy({ where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, msg: 'Data Voucher Berhasil di hapus' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Voucher Gagal di hapus' })
            })
    },
    listOne(req, res) {
        Voucher
            .find({ where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Voucher tidak di temukan' })
            })
    },
    listByKatagori(req, res) {
        Voucher
            .find({ where: { fk_kategori: req.params.id } }).then(result => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Voucher tidak di temukan' })
            })
    },
    listAll(req, res) {
        Voucher
            .findAll().then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
    listByKatagori(req, res) {
        Voucher
            .findAll({ where: { fk_kategori: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    }
};