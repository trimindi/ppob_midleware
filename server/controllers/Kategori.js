const Kategori = require('../models').Kategori;
const Voucher = require('../models').Voucher;

module.exports = {
    create(req, res) {
        Kategori
            .create(req.body).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Kategori Berhasil di buat' })
            })
    },
    update(req, res) {
        Kategori
            .update(req.body, { where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Kategori Berhasil di Update' })
            })
    },
    delete(req, res) {
        Kategori
            .destroy({ where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Kategori Berhasil di hapus' })
            })
    },
    listOne(req, res) {
        Kategori
            .find({ where: { 'id': req.params.id } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
    listByGolongan(req, res) {
        Kategori.findAll({ where: { golongan: req.params.golongan } }).then(result => {
            res.status(200).json({
                status: true,
                data: result
            });
        }).catch(err => {
            res.status(200).json({
                status: false,
                msg: 'Data tidak ditemukan'
            });
        });
    },
    listAll(req, res) {
        Kategori
            .findAll().then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
};