const Saldo = require('../models').Saldo;

module.exports = {
    create(req, res) {
        Saldo
            .create(req.body).then((result) => {
                res.status(200).json({ status: true, msg: 'Data user berhasil di update' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data user gagal di buat' })
            })
    },
    update(req, res) {
        Saldo
            .update(req.body, { where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, msg: 'Data user berhasil di update' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data user Gagal di update' })
            })
    },
    listOne(req, res) {
        Saldo
            .find({ where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
    listAll(req, res) {
        Saldo
            .findAll().then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
    deposit(req, res) {
        Saldo
            .find({ where: { id: req.body.id } }).then(result => {
                result.saldo = result.saldo + req.body.nominal;
                Saldo.update({ saldo: result.saldo }, { where: { id: req.body.id } }).then(result => {
                    res.status(200).json({
                        status: true,
                        msg: "Deposit Saldo berhasil di Tambah"
                    })
                }).catch(err => console.log(err));
            }).catch(err => console.log(err));
    }
};