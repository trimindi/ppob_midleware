const Rekap = require('../models').Rekap;
const Voucher = require('../models').Voucher;
const Transaksi = require('../models').Transaksi;
const Saldo = require('../models').Saldo;
const Users = require('../models').Users;
const md5 = require('md5');
const config = require('../../config');
const unirest = require('unirest');

var topup = (mssidn, denom, ntrans) => {
    return new Promise((resolve, reject) => {
        let req = unirest("GET", config.baseUrl);
        req.query({
            "regid": ntrans,
            "userid": config.userid,
            "passwd": config.password,
            "msisd": mssidn,
            "denom": denom
        });

        req.headers({
            "cache-control": "no-cache"
        });

        req.end(function(res) {
            if (res.error) return reject(res.error);
            return resolve(res.body);
        });
    })
}

module.exports = {
    inquiry(req, res) {
        // req.checkBody('kode', 'invalid kode params').notEmpty();
        // req.checkBody('mssidn', 'invalid mssidn params').notEmpty();
        // req.sanitize('kode').toBoolean();
        // req.sanitize('mssidn').toBoolean();

        // req.getValidationResult().then(function(result) {
        //     if (!result.isEmpty()) {
        //         res.send('There have been validation errors: ' + util.inspect(result.array()), 400);
        //         return;
        //     }
        // });
        // topup(req.body.mssidn, 'CIPLN' + req.body.kode, req.ntrans)
        //     .then(result => {
        //         let obj = JSON.parse(result);
        //         Rekap
        //             .create({
        //                 ntrans: req.ntrans,
        //                 response: result,
        //                 mssidn: req.body.mssidn,
        //                 kode: req.body.kode,
        //                 status: obj.status
        //             }).then((res) => {


        //             }).catch((err) => {

        //             })
        //         res.status(200).send(result);
        //     }).catch((err) => {
        //         console.log(err);
        //         res.status(304).json({
        //             'status': 5,
        //             'massage': 'Terjadi Masalah dengan server'
        //         });
        //     });
        let data = JSON.parse('{"status":1,"timestamp":20170217000000,"tn":"12312","reg_id":"12312","kode_produk":"CIPLN20","sn":"0","message":"SUGIONO.12312","harga":"0","saldo":"0"}');

        data.reg_id = req.ntrans;
        data.message = undefined;
        data.kode_produk = 'CIPLN' + req.body.kode;
        data.msg = req.body.mssidn + 'Ahmad Sukri Abdillah';
        res.status(200).json({
            status: true,
            data: data
        });



    },
    pulsa(req, res) {
        var saldo;
        var voucher;
        Users.find({ where: { id: req.decoded.id } }).then(result => {
            var users = result;
            var harga;
            Voucher.find({ where: { kode: req.body.kode, fk_kategori: req.body.kategori } })
                .then(result => {
                    voucher = result;
                    harga = result.harga + result.fee;
                    Saldo.find({ where: { id: '0000000002' } }).then(result => {
                        saldo = result;
                    });
                    if (result.harga > saldo.saldo) {
                        res.status(200).json({
                            status: false,
                            msg: "terjadi masalah dengan server harap hubungu CS"
                        });
                        return;
                    }
                    if (users.saldo < (result.harga + result.fee)) {
                        res.status(200).json({
                            status: false,
                            msg: "saldo anda tidak mencukupi untuk melakukan transaksi Rp." + users.saldo
                        });
                        return;
                    }
                    Transaksi
                        .create({
                            ntrans: req.ntrans,
                            nominal: result.harga + result.fee,
                            mssidn: req.body.mssidn,
                            fk_user: req.decoded.id,
                            kode: req.body.kode,
                            keterangan: result.nama,
                            pembayaran: req.body.pembayaran,
                            kembali: req.body.kembali,
                            fee_loket: result.fee_loket,
                            fee_agent: result.fee,
                            st: 0
                        }).then(result => {
                            topup(req.body.mssidn, req.body.kode, req.ntrans)
                                .then(result => {
                                    let data = JSON.parse(result);
                                    Transaksi
                                        .update({
                                            st: data.status,
                                            msg: data.message
                                        }, { where: { ntrans: req.ntrans } }).then(res => {
                                            if (data.status <= 2) {
                                                let saldo = users.saldo - harga;
                                                Users
                                                    .update({
                                                        saldo: saldo
                                                    }, { where: { id: req.decoded.id } })
                                                    .then(result => {

                                                    }).catch(err => console.log(err));
                                                Saldo.update({
                                                    saldo: saldo.saldo - voucher.saldo

                                                }, { where: { id: '0000000002' } });
                                            }
                                        }).catch(err => console.log(err));
                                    Rekap
                                        .create({
                                            ntrans: req.ntrans,
                                            response: result,
                                            mssidn: req.body.mssidn,
                                            kode: req.body.kode,
                                            status: data.status
                                        }).then((result) => {

                                        }).catch((err) => {
                                            console.log(err)
                                        });
                                    let reposnse = JSON.parse(result);
                                    res.status(200).json({
                                        status: reposnse.status,
                                        timestamp: reposnse.timestamp,
                                        tn: reposnse.tn,
                                        reg_id: reposnse.reg_id,
                                        sn: reposnse.sn,
                                        msg: reposnse.message,
                                        harga: reposnse.harga
                                    });
                                }).catch((err) => {
                                    console.log(err);
                                    res.status(304).json({
                                        'status': 5,
                                        'massage': 'Terjadi Masalah dengan server'
                                    });
                                });

                        }).catch(err => console.log(err));
                }).catch(err => console.log(err));
        }).catch(err => console.log(err));
    },
    payment(req, res) {

        // req.checkBody('kode', 'invalid kode params').notEmpty();
        // req.checkBody('mssidn', 'invalid mssidn params').notEmpty();
        // req.getValidationResult().then(function(result) {
        //     if (!result.isEmpty()) {
        //         res.send('There have been validation errors: ' + util.inspect(result.array()), 400);
        //         return;
        //     }
        // });
        // var harga;
        // var users;
        // var voc;
        // var rekap;
        // Rekap
        //     .find({
        //         where: {
        //             ntrans: req.body.ntrans,
        //         }
        //     }).then(result => {
        //         let obj = JSON.parse(result.response);
        //         rekap = result;
        //         Users.find({ where: { id: req.decoded.id } }).then(result => {
        //             users = result;
        //             Voucher.find({ where: { kode: req.body.kode, fk_kategori: req.body.kategori } }).then(result => {
        //                 harga = result.harga + result.fee;
        //                 voc = result;
        //                 if (harga < users.saldo) {
        //                     res.status(200).json({
        //                         status: false,
        //                         msg: "saldo anda tidak mencukupi untuk melakukan transaksi Rp." + users.saldo
        //                     })
        //                     return;
        //                 }
        //             }).catch(err => console.error(err));
        //         }).catch(err => console.error(err));
        //         topup(result.mssidn, 'BIPLN' + obj.kode, obj.reg_id)
        //             .then(result => {
        //                 let obj = JSON.parse(result);
        //                 if (obj.status <= 2) {
        //                     Users.update({
        //                         saldo: users.saldo - harga
        //                     }, { where: { id: req.decoded.id } }).then(result => {

        //                     }).catch(err => console.error(err));
        //                 }
        //                 Transaksi.create({
        //                     ntrans: rekap.ntrans,
        //                     nominal: harga,
        //                     mssidn: rekap.mssidn,
        //                     fk_user: req.decoded.id,
        //                     kode: voc.kode,
        //                     keterangan: voc.nama,
        //                     kembali: 0,
        //                     fee_loket: voc.fee_loket,
        //                     fee_agent: voc.fee,
        //                     st: obj.status
        //                 }).then(result => {

        //                 }).catch(err => console.error(err));
        //                 Rekap
        //                     .update({
        //                         ntrans: rekap.ntrans,
        //                         response: result,
        //                         mssidn: rekap.mssidn,
        //                         kode: rekap.kode,
        //                         status: obj.status
        //                     }).then((res) => {

        //                     }).catch((err) => {

        //                     })
        //                 res.status(200).send(result);
        //             }).catch((err) => {
        //                 console.log(err);
        //                 res.status(304).json({
        //                     'status': 5,
        //                     'massage': 'Terjadi Masalah dengan server'
        //                 });
        //             });

        //     }).catch(err => console.log(err));
        let data = JSON.parse('{"status":1,"timestamp":20170217000000,"tn":"000000","reg_id":"12312","kode_produk":"BIPLN20","sn":"1234-0987-6543-2109","message":"86012266226 SUKSES NomorToken =1234-0987-6543-2109-0242 Ket=DUMMY TESTING MAULANA/R1/1300/12,9KWH.BIPLN20","harga":"0","saldo":"0"}');

        data.reg_id = req.body.ntrans;
        data.kode_produk = 'BIPLN' + req.body.kode;
        data.message = req.body.mssidn + 'SUKSES NomorToken =1234-0987-6543-2109-0242 Ket=DUMMY TESTING MAULANA/R1/1300/12,9KWH.' + data.kode_produk;
        res.status(200).json({
            status: true,
            data: data
        });
    },
    paketdata(req, res) {
        var saldo;
        var voucher;
        Users.find({ where: { id: req.decoded.id } }).then(result => {
            var users = result;
            var harga;
            Voucher.find({ where: { kode: req.body.kode, fk_kategori: req.body.kategori } })
                .then(result => {
                    voucher = result;
                    harga = result.harga + result.fee;
                    Saldo.find({ where: { id: '0000000002' } }).then(result => {
                        saldo = result;
                    });
                    if (result.harga > saldo.saldo) {
                        res.status(200).json({
                            status: false,
                            msg: "terjadi masalah dengan server harap hubungu CS"
                        });
                        return;
                    }
                    if (users.saldo < (result.harga + result.fee)) {
                        res.status(200).json({
                            status: false,
                            msg: "saldo anda tidak mencukupi untuk melakukan transaksi Rp." + users.saldo
                        });
                        return;
                    }
                    Transaksi
                        .create({
                            ntrans: req.ntrans,
                            nominal: result.harga + result.fee,
                            mssidn: req.body.mssidn,
                            fk_user: req.decoded.id,
                            kode: req.body.kode,
                            keterangan: result.nama,
                            pembayaran: req.body.pembayaran,
                            kembali: req.body.kembali,
                            fee_loket: result.fee_loket,
                            fee_agent: result.fee,
                            st: 0
                        }).then(result => {
                            topup(req.body.mssidn, req.body.kode, req.ntrans)
                                .then(result => {
                                    let data = JSON.parse(result);
                                    Transaksi
                                        .update({
                                            st: data.status,
                                            msg: data.message
                                        }, { where: { ntrans: req.ntrans } }).then(res => {
                                            if (data.status <= 2) {
                                                let saldo = users.saldo - harga;
                                                Users
                                                    .update({
                                                        saldo: saldo
                                                    }, { where: { id: req.decoded.id } })
                                                    .then(result => {

                                                    }).catch(err => console.log(err));
                                                Saldo.update({
                                                    saldo: saldo.saldo - voucher.saldo

                                                }, { where: { id: '0000000002' } });
                                            }
                                        }).catch(err => console.log(err));
                                    Rekap
                                        .create({
                                            ntrans: req.ntrans,
                                            response: result,
                                            mssidn: req.body.mssidn,
                                            kode: req.body.kode,
                                            status: data.status
                                        }).then((result) => {

                                        }).catch((err) => {
                                            console.log(err)
                                        });
                                    let reposnse = JSON.parse(result);
                                    res.status(200).json({
                                        status: reposnse.status,
                                        timestamp: reposnse.timestamp,
                                        tn: reposnse.tn,
                                        reg_id: reposnse.reg_id,
                                        sn: reposnse.sn,
                                        msg: reposnse.message,
                                        harga: reposnse.harga
                                    });
                                }).catch((err) => {
                                    console.log(err);
                                    res.status(304).json({
                                        'status': 5,
                                        'massage': 'Terjadi Masalah dengan server'
                                    });
                                });

                        }).catch(err => console.log(err));
                }).catch(err => console.log(err));
        }).catch(err => console.log(err));
    },
};