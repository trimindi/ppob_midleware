const Users = require('../models').Users;
const md5 = require('md5');
const Cif = require('../models').Cif;

const jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens

const config = require('../config/config.json');
module.exports = {
    create(req, res) {
        Users
            .create(req.body).then((result) => {
                res.status(200).json({ status: true, msg: 'Data user berhasil di update' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data user gagal di buat' })
            })
    },
    update(req, res) {
        Users
            .update(req.body, { where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, msg: 'Data user berhasil di update' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data user Gagal di update' })
            })
    },
    delete(req, res) {
        Users
            .destroy({ where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, msg: 'Data User Berhasil di hapus' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data User Gagal di hapus' })
            })
    },
    listOne(req, res) {
        Users
            .find({ where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
    listAll(req, res) {
        Users
            .findAll().then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
    deposit(req, res) {
        Users.find({ where: { id: req.params.id } }).then((result) => {
            result.saldo = result.saldo + req.params.deposit;
            Users
                .update({ saldo: result.saldo }, { where: { id: req.params.id } }).then((result) => {
                    res.status(200).json({ status: true, data: 'Deposit Behasil.' })
                }).catch((err) => {
                    res.status(200).json({ status: false, msg: 'Deposit Gagal' })
                })
        })

    },
    auth(req, res) {
        Users.find({ where: { username: req.body.username } }).then(result => {
            if (result == null) {
                res.status(200).json({
                    status: false,
                    msg: "username / password anda salah"
                });
                return;
            }
            if (md5(req.body.password) == result.password) {
                let data = {
                    id: result.id,
                    username: result.username,
                    is_admin: result.is_admin,
                }
                let token = jwt.sign(data, 'sukrigantengsekali', {
                    expiresIn: '3h'
                });
                let users = {
                    username: result.username,
                    first_name: result.first_name,
                    last_name: result.last_name,
                    email: result.email,
                    saldo: result.saldo
                }
                Cif.find({ where: { cif: result.cif } })
                    .then(result => {
                        res.status(200).json({
                            status: true,
                            is_admin: result.is_admin,
                            user: users,
                            cif: result,
                            token: token
                        });
                    }).catch(err => {
                        res.status(200).json({
                            status: true,
                            is_admin: result.is_admin,
                            user: users,
                            cif: null,
                            token: token
                        });
                    });
            } else {
                res.status(401).json({
                    status: false,
                    msg: "Username / Password anda Salah"
                });
            }
        }).catch(err => console.error(err));
    },
    chpass(req, res) {
        Users
            .update({ password: md5(req.body.password) }, { where: { id: req.body.userid } }).then(result => {
                res.status(200).json({
                    status: true,
                    msg: "Password berhasil di ubah"
                })
            }).catch(err => console.log(err));

    },
    deposit(req, res) {
        Users
            .find({ where: { id: req.body.userid } }).then(result => {
                result.saldo = result.saldo + req.body.nominal;
                Users.update({ saldo: result.saldo }, { where: { id: req.body.userid } }).then(result => {
                    res.status(200).json({
                        status: true,
                        msg: "Deposit Saldo berhasil di Tambah"
                    })
                }).catch(err => console.log(err));
            }).catch(err => console.log(err));
    }
};