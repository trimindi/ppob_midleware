const middleware = require('./middleware');
const users = require('./Users');
const kategori = require('./Kategori');
const voucher = require('./Voucher');
const mutasi = require('./Mutasi');
const transaksi = require('./Transaksi');
const rekap = require('./Rekap');
const pembayaran = require('./pembayaran');
const saldo = require('./saldo');
const pulsamatic = require('./pulsamatic');
const voucher_loket = require('./Voucher_loket');



module.exports = {
    middleware,
    users,
    kategori,
    voucher,
    mutasi,
    rekap,
    saldo,
    transaksi,
    voucher_loket,
    pulsamatic,
    pembayaran
};