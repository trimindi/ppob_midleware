const Mutasi = require('../models').Mutasi;

module.exports = {
    create(req, res) {
        Mutasi
            .create(req.body).then((result) => {
                res.status(200).json({ status: true, msg: 'Data deposit berhasil di update' })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data deposit gagal di buat' })
            })
    },
    listOne(req, res) {
        Mutasi
            .find({ where: { id: req.params.id } }).then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    },
    listAll(req, res) {
        Mutasi
            .findAll().then((result) => {
                res.status(200).json({ status: true, data: result })
            }).catch((err) => {
                res.status(200).json({ status: false, msg: 'Data Tidak Ditemukan' })
            })
    }

};