const middleware = require("../controllers").middleware;
const kategori = require("../controllers").kategori;
const voucher = require("../controllers").voucher;
const mutasi = require("../controllers").mutasi;
const users = require("../controllers").users;
const transaksi = require("../controllers").transaksi;
const rekap = require("../controllers").rekap;
const pembayaran = require("../controllers").pembayaran;
const saldo = require("../controllers").saldo;
const pulsamatic = require("../controllers").pulsamatic;


const jwt = require('jsonwebtoken');
const gn = require("../helper/helper.js");
module.exports = (app) => {

    app.use(function(req, res, next) {


        var token = req.body.token || req.query.token || req.headers['authorization'];
        if (token) {

            // verifies secret and checks exp
            jwt.verify(token, app.get('secret'), function(err, decoded) {
                if (err) {
                    return res.status(401).json({ success: false, message: err.message });
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded;
                    next();
                }
            });

        } else {
            return res.status(401).send({
                success: false,
                message: 'UNAUTHORIZATION REQUEST'
            });

        }
    });

    app.get('/voucher', voucher.listAll);
    app.get('/voucher/:id', voucher.listByKatagori);
    app.get('/voucher/detail/:id', voucher.listOne);
    app.post('/voucher', voucher.create);
    app.put('/voucher/:id', voucher.update);
    app.delete('/voucher/:id', voucher.delete);

    app.get('/kategori', kategori.listAll);
    app.get('/kategori/:golongan', kategori.listByGolongan);
    app.post('/kategori', kategori.create);
    app.put('/kategori/:id', kategori.update);
    app.delete('/kategori/:id', kategori.delete);

    app.get('/transaksi', transaksi.listAll);
    app.get('/transaksi/:id', transaksi.listOne);
    app.post('/transaksi', transaksi.create);

    app.get('/rekap', rekap.listAll);
    app.get('/rekap/:id', rekap.listOne);

    app.get('/deposit', mutasi.listAll);
    app.get('/deposit/:id', mutasi.listOne);
    app.post('/deposit', mutasi.create);

    app.get('/saldo', saldo.listAll);
    app.get('/saldo/:id', saldo.listOne);
    app.post('/saldo', saldo.create);
    app.put('/saldo', saldo.update);
    app.post('/saldo/deposit', saldo.deposit);


    app.get('/users', users.listAll);
    app.get('/users/:id', users.listOne);
    app.post('/users', users.create);
    app.put('/users/chpass', users.chpass);
    app.post('/users/deposit', users.deposit);
    app.put('/users/:id', users.update);
    app.delete('/users/:id', voucher.delete);

    app.use(function(req, res, next) {
        gn.generateNtrans().then((result) => {
            req.ntrans = result;
            next();
        });
    })


    app.post('/ppob/pulsamatic/payment', pulsamatic.payment);

    app.post('/ppob/pembayaran/inquiry', pembayaran.inquiry);
    app.post('/ppob/pembayaran/payment', pembayaran.payment);
    app.post('/ppob/inquiry', middleware.inquiry);
    app.post('/ppob/payment', middleware.payment);
    app.post('/ppob/pulsa', middleware.pulsa);
    app.post('/ppob/paketdata', middleware.paketdata);
};
