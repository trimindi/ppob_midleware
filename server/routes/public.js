const users = require("../controllers").users;
var parser = require('xml2json');
var checkStatus = (status) => {
    switch (status) {
        case 1:
            return 'SUKSES';
            break;
        case 2:
            return 'Sedang Dalam Proses';
            break;
        case 3:
            return 'Gagal Transaksi';
            break;
        case 4:
            return 'Gagal Saldo Tidak Cukup';
            break;
        case 5:
            return 'Double Transaksi';
            break;
        case 6:
            return 'UserID/Passwd Salah';
            break;
        case 7:
            return 'SUSPECT';
            break;

    }
}
module.exports = (app) => {

    app.post('/confirmation/recon', function(req, res) {
        if (req.body.reg_id == undefined || isJSON(req.body)) {
            res.json({
                message: 'required param not provided'
            });
            return;
        }
        if (req.body.status != 1) {
            Transaksi.find({ ntrans: req.body.reg_id }).then(result => {
                let kembali = result.nominal + result.fee_agent + result.admin;
                Users.find({ where: { id: result.fk_user } }).then(result => {
                    kembali = kembali + result.saldo;
                    Users.update({ saldo: kembali }, { where: { id: result.id } }).then(result => {
                        Transaksi.update({ st: 2 }, { where: { ntrans: req.body.reg_id } }).then(result => {}).catch(err => console.log(err));
                    }).catch(err => console.log(err));
                }).catch(err => console.log(err));
            }).catch(err => console.log(err));
        }

        Rekap
            .update({
                response: req.body,
                status: checkStatus(req.body.status)
            }, { where: { ntrans: req.body.reg_id } }).then((res) => {
                Transaksi.update({
                    st: (req.body.status == 1 ? 1 : (req.body.status == 2 ? 0 : 2)),
                }, { where: { ntrans: req.body.reg_id } }).then(res => {

                }).catch(err => console.log(err));
                res.json({
                    status: true,
                    message: 'sukses update status transaksi , terima kasih'
                });

            }).catch((err) => {
                res.json({
                    status: false,
                    message: 'Gagal Update Status'
                });

            })


    });

    app.post('/auth', users.auth);
};