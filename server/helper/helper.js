var Transaksi = require('../models').Transaksi;

exports.generateNtrans = () => {
    return new Promise((resolve, reject) => {
        let date = new Date();
        let rand = Math.floor((Math.random() * 10) + Math.random() * 5);
        let condition = false;
        let ntrans = "" + date.getFullYear() + date.getDate() + (date.getMonth() + 1) + date.getMilliseconds() + rand;
        do {
            Transaksi.find({ where: { ntrans: ntrans } })
                .then((res) => {
                    if (res == null) {
                        condition = true;
                        resolve(ntrans);
                    }
                    rand = Math.floor((Math.random() * 10) + Math.random() * 5);
                    ntrans = "" + date.getFullYear() + date.getDate() + (date.getMonth() + 1) + date.getMilliseconds() + rand;
                }).catch((err) => {
                    console.log(err);
                });
        } while (condition);
    })
}